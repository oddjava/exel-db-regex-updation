package exceltodb;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;

public class ExcelToDB 
{

	static MongoClient mongoClient = new MongoClient("178.32.49.5",27017);
	static DB db = mongoClient.getDB("crawler");
	static DBCollection seedColl = db.getCollection("SeedUrl");
	static DBCollection domaincoll = db.getCollection("domain");
	static SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

	public static void main(String[] args) 
	{
		SaveFile(".\\DB Sheet.xlsx");	
	}

	public static void SaveFile(String path) 
	{
		int duplicate=0;
		int inserted=0;
		int upsent=0;

		String ext1 = FilenameUtils.getExtension(path);
		System.out.println(ext1);
		BasicDBObject object = new BasicDBObject();

		Workbook wb_xssf; //Declare XSSF WorkBook 
		Workbook wb_hssf; //Declare HSSF WorkBook 
		Sheet sheet=null; //sheet can be used as common for XSSF and HSSF WorkBook 		
		Cell cell1,cell2,cell3,cell4,cell5,cell6,cell7,cell8;

		String domain,SeedUrl,Regex,type,moduleName,systemName,bucketNumber;
		boolean status = false; 
		Row nextRow;
		Iterator<Cell> cellIterator;
		Cursor val;

		try {

			

			FileInputStream inputStream = new FileInputStream(path);
			
			if(ext1.equalsIgnoreCase("xls"))
			{
				wb_hssf = new HSSFWorkbook(inputStream);
				sheet = wb_hssf.getSheetAt(0);	//Domain present but Seed absent sheet
			}
			else if (ext1.equalsIgnoreCase("xlsx"))
			{
				wb_xssf = new XSSFWorkbook(inputStream);      
				sheet = wb_xssf.getSheetAt(0);             //Domain present but Seed absent sheet                                             
			}


			Iterator<Row> iterator = sheet.iterator();

			int count=1;
			
			iterator.next();
			while (iterator.hasNext()) 
			{
				System.out.println("Count= "+(++count));
				nextRow = iterator.next();
				cellIterator = nextRow.cellIterator();

				if (cellIterator.hasNext()) 
				{
					cell1 = cellIterator.next();
					cell2 = cellIterator.next();
					cell3 = cellIterator.next();
					cell4 = cellIterator.next();
					cell5 = cellIterator.next();
					cell6 = cellIterator.next();
					cell7 = cellIterator.next();
					cell8 = cellIterator.next();	
					
					domain = cell1.getStringCellValue();
					
					try
					{
						status =  cell2.getBooleanCellValue();
						
						SeedUrl = cell3.getStringCellValue();
						Regex =	cell4.getStringCellValue();
						type = cell5.getStringCellValue();	
						moduleName	= cell6.getStringCellValue();
						systemName = cell7.getStringCellValue();
						bucketNumber =""+cell8.getNumericCellValue();
						
						if(status)  //Update in seedurl
						{
							if(!domain.isEmpty())
							{
								System.out.print(domain+" ");
								System.out.print(status+" ");
								System.out.print(SeedUrl+" ");
								System.out.print(Regex+" ");
								System.out.print(type+" ");
								System.out.print(moduleName+" ");
								System.out.print(systemName+" ");
								System.out.println(bucketNumber+" ");
								
								object.clear();
								object.append("domainUrl", domain);
								object.append("url", SeedUrl);
								object.append("type", type);
								object.append("moduleName", moduleName);
								object.append("addedDate", ft.format(new Date()));
								object.append("systemName", systemName);
								object.append("bucketNumber", bucketNumber);
								
								System.out.println(object.toJson());
								
								try
								{								
									DBObject object2 = domaincoll.findOne(new BasicDBObject("domainName",domain));
									if(object2!=null)
									{										
										domaincoll.update(new BasicDBObject("domainName",domain),new BasicDBObject("$set",new BasicDBObject("regex",Regex)));
										try
										{
											seedColl.insert(object);
											inserted++;
											System.out.println("Updated in Domain and Seed Collection...!");
										}
										catch(DuplicateKeyException de)
										{
											duplicate++;
											System.out.println("Seed Url duplicate");
										}										
									}
									else
									{
										upsent++;
										System.out.println("Domain not present in DB");
									}	
								}
								catch(Exception e)
								{ 
									System.out.println(e);
									e.printStackTrace();
								}

							}
						}
						else  //remove from domain name
						{
							domaincoll.remove(new BasicDBObject("domainName",domain));
							System.out.println("Delete from domain table");
						}						
					}
					catch(Exception e)
					{
						System.out.println("Status column absent in xls sheet.");
					}					
				}
				
				System.out.println();
				System.out.println("Duplicate= "+duplicate+"\nInserted="+inserted+"\nUpsent="+upsent);
				System.out.println();
			}

			
			inputStream.close();

		} catch (Exception e) {

			System.err.println("exception in reading excel");
			System.err.println("Please provide valid path.. at line : 42");

			e.printStackTrace();

		}
	}
}
